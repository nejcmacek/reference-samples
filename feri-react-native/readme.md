# FERI React Native

This project was a part of Web Programming and Compiling Programming Languages courses.

The project features a ReactNative application and Node.js backend used for detection of web changes. The project was demonstrative.

For more information about the project, please do not hesitate to contact me.
