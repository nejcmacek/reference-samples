# SpaceHunt Project

This is a collaborative academic project. It is only an extract of my own code and is therefore not runnable.

The project features a custom JSX rendering engine and example usage, custom-written build system with documentation, and some other components. Link below will lead you to the related source code files.

- [Custom JSX Rendering Engine](./src/InterfaceBuilder.ts)
- [JSX Engine example usage](./src/Scenes/Intro/Intro.tsx)
- [JSX Engine Documentation](./doc/dom-manipulation.md)
- [Documentation directory](./doc)
- [Build configuration directory](./config)

For more information about the project, please do not hesitate to contact me.
