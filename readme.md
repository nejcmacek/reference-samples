# Nejc Maček - Reference Sample

This repository contains reference samples of my previous experience.

- [React Native Project](./feri-react-native)
- [MS Cognitive Services Project](./feri-face-recognition)
- [JSX Custom Rendering Engine Project](./tut-jsx-engine)

Other references include:

- [Personal website](https://nejo.si/)
- [OpenScience alternative backend](http://openscience.si/iskanjeNejcMacek/)
- [Vue.js test PWA](http://nejo.si/internal/randomizer/)
- [Other apps](http://apps.nejo.si/)

Please also see the [attached CV](./cv.pdf) for more details.

## Contact

If any questions occur, please do not hesitate to contact me.

- Email: [macek.nejc@gmail.com](mailto:macek.nejc@gmail.com)
- Website: [nejo.si](https://nejo.si/)
- LinkedIn: [nejc-macek](https://www.linkedin.com/in/nejc-macek/)
- Facebook: [macek.nejc](https://www.facebook.com/macek.nejc)
