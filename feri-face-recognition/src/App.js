import React, { Component } from 'react'
import './App.css'
import PageBody from './views/PageBody'
import Settings from './views/Settings'
import Results from './views/Results'

class App extends Component {

  state = {
    settings: false,
    results: false
  }

  render() {
    const { settings, results } = this.state
    return (
      <div>
        <header>
          <span className="wrapper wrapper-padded page-title">Multimedia Face Recognition Project</span>
          <div
            className="menu-button fa fa-chart-line"
            onClick={() => this.setState({ results: !results, settings: false })}
          />
          <div
            className="menu-button fa fa-bars"
            onClick={() => this.setState({ settings: !settings, results: false })}
          />
        </header>
        <PageBody blur={settings || results} />
        {settings && <Settings
          onClose={() => this.setState({ settings: false })}
        />}
        {results && <Results
          onClose={() => this.setState({ results: false })}
        />}
      </div>
    )
  }
}

export default App
