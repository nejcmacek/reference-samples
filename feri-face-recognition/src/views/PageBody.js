import React, { Component } from 'react'
import FaceSquare from '../components/FaceSquare'
import './PageBody.css'

const url = 'https://feri.um.si/site/assets/files/4584/22790169_10211625957298639_207584449_o.jpg'
const img = new Image()
img.src = url

export default class PageBody extends Component {

	fired() {
		console.log('unimplemented handling of manually fired fab')
	}

	async componentDidMount() {
		if (!img.complete)
			await new Promise((resolve, reject) => img.addEventListener('load', resolve))
		const { canvas } = this
		const context = canvas.getContext('2d')
		context.drawImage(img, 0, 0, canvas.width, canvas.height)
	}

	render() {
		const { blur } = this.props
		const blurfilter = {
			filter: 'blur(5px)'
		}
		return (
			<div className="page page-body" style={blur ? blurfilter : null}>
				<canvas id="canvas" ref={elt => this.canvas = elt} />
				<div className="squares">
					<FaceSquare
						left={120}
						top={56}
						width={100}
						height={100}
						emotion="Emotionless"
						strength={0.5}
					/>
					<FaceSquare
						left={0.5}
						top={0.6}
						width={0.2}
						height={0.2}
						emotion="Sentimental"
						strength={0.2}
						usePercentage
					/>
				</div>
				<div
					className="fab"
					onClick={this.fired.bind(this)}
				>
					<span className="fa fa-camera"></span>
				</div>
			</div>
		)
	}

}
