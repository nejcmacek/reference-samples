import React, { Component } from 'react'

export default class Settings extends Component {

	get value() {
		return this.state.value
	}

	state = {
		value: 0
	}

	render() {
		const { onClose } = this.props
		const { value } = this.state
		return (
			<div className="page settings content-wrapper">
				<p>Timer:
					<span className="small"> (set to 0 to disable)</span>
				</p>
				<p>
					<input
						type="text"
						value={value}
						onChange={e => this.setState({ value: e.target.value })}
					/>
				</p>
				<div className="right">
					<button onClick={onClose}>Close</button>
				</div>
			</div >
		)
	}

}
