import React, { Component } from 'react'

export default class Results extends Component {

	render() {
		const { onClose } = this.props
		return (
			<div className="page results content-wrapper">
				<p>Display results here</p>
				<div className="right">
					<button onClick={onClose}>Close</button>
				</div>
			</div >
		)
	}

}
