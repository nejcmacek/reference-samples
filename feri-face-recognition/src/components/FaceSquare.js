import React, { Component } from 'react'
import './FaceSquare.css'

export default class FaceSquare extends Component {

	render() {
		const { left, top, height, width, emotion, strength, usePercentage } = this.props
		const style = usePercentage
			? {
				left: (left || 0) * 100 + '%',
				top: (top || 0) * 100 + '%',
				width: (width || 0) * 100 + '%',
				height: (height || 0) * 100 + '%'
			} : {
				left,
				top,
				width,
				height
			}
		return (
			<div
				className="face-square"
				style={style}
			>
				<div className="emotion-display-triangle"></div>
				<div className="emotion-display">
					<span className="emotion-text">
						{emotion}: {Math.round(strength * 100)}%
					</span>
				</div>
			</div>
		)
	}

}
